import images
from PIL import Image, ImageFilter

def change_colors(image: list[list[tuple[int, int, int]]], to_change: tuple[int, int, int], to_change_to: tuple[int, int, int]) -> list[list[tuple[int, int, int]]]:
    imagen = []
    for lista in image:
        listo = []
        for tupla in lista:
            if tupla == to_change:
                listo.append(to_change_to)
            else:
                listo.append(tupla)
        imagen.append(listo)

    return imagen


def rotate_right(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    width, height = images.size(image)
    pixels = []
    for lista in image:
        for tupla in lista:
            pixels.append(tupla)

    pixels_xy = []
    for x in range(height):
        pixels_xy.append([0] * width)
        for y in range(width):
            pixels_xy[x][y] = pixels[(y * height) + x]
    return pixels_xy


def mirror(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    size = len(image)
    counter = 0
    tamano = size - 1
    for num in range(size - 1):
        counter += 1
        if counter <= size // 2:
            image[num], image[tamano] = image[tamano], image[num]
            tamano -= 1

    return image


def rotate_colors(image: list[list[tuple[int, int, int]]], increment: int) -> list[list[tuple[int, int, int]]]:
    for lista in image:
        for tupla in lista:
            num_lista = image.index(lista)
            num_tupla = lista.index(tupla)
            tup1 = tupla[0] + increment
            tup2 = tupla[1] + increment
            tup3 = tupla[2] + increment
            if tup1 > 256:
                num = tup1 / 256
                value = tup1 // 256
                numero = num - value
                tup1 = int(256 * numero)
            if tup2 > 256:
                num = tup2 / 256
                value = tup2 // 256
                numero = num - value
                tup2 = int(256 * numero)
            if tup3 > 256:
                num = tup3 / 256
                value = tup3 // 256
                numero = num - value
                tup3 = int(256 * numero)

            tup1 = abs(tup1)
            tup2 = abs(tup2)
            tup3 = abs(tup3)
            image[num_lista][num_tupla] = (tup1, tup2, tup3)

    return image


def blur(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    height = len(image)
    width = len(image[0])

    imagen_blur = [[(0, 0, 0) for _ in range(width)] for _ in range(height)]

    for y in range(height):
        for x in range(width):
            total_pixels = 0
            total_sum = [0, 0, 0]

            for dy in range(-4, 5):
                for dx in range(-4, 5):
                    ny, nx = y + dy, x + dx

                    if 0 <= ny < height and 0 <= nx < width:
                        total_pixels += 1
                        total_sum[0] += image[ny][nx][0]
                        total_sum[1] += image[ny][nx][1]
                        total_sum[2] += image[ny][nx][2]

            image[y][x] = (
                total_sum[0] // total_pixels,
                total_sum[1] // total_pixels,
                total_sum[2] // total_pixels
            )

    return image



def shift(image: list[list[tuple[int, int, int]]], horizontal: int, vertical: int) -> list[list[tuple[int, int, int]]]:
    width, height = images.size(image)
    num = abs(horizontal)
    nums = abs(vertical)
    listas1 = images.create_blank(num, height)
    if horizontal >= 0:
        imagen = listas1 + image

    else:
        imagen = image + listas1

    for lista in imagen:
        for xx in range(nums):
            if vertical >= 0:
                lista.append((0, 0, 0))
            else:
                lista.insert(0, (0, 0, 0))

    return imagen
def crop(image: list[list[tuple[int, int, int]]], x: int, y: int, width: int, height: int) -> list[list[tuple[int, int, int]]]:
    max_x = x + width
    max_y = y + height
    imagen = []
    for filas in range(x, max_x):
        lista = []
        for columnas in range(y, max_y):
            valor = image[filas][columnas]
            lista.append(valor)

        imagen.append(lista)

    return imagen


def grayscale(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    imagen = []
    for lista in image:
        lista1 = []
        for tupla in lista:
            nuevo_val = int((tupla[0] + tupla[1] + tupla[2]) // 3)
            if nuevo_val > 255:
                nuevo_val = 255
            nueva_tupla = (nuevo_val, nuevo_val, nuevo_val)
            lista1.append(nueva_tupla)

        imagen.append(lista1)
    return imagen

def filter(image: list[list[tuple[int, int, int]]], r: float, g: float, b: float) -> list[list[tuple[int, int, int]]]:
    imagen = []
    for lista in image:
        listo = []
        for tupla in lista:
            tup1 = tupla[0] * r
            tup2 = tupla[1] * g
            tup3 = tupla[2] * b
            if tup1 > 255:
                tup1 = 255
            if tup2 > 255:
                tup2 = 255
            if tup3 > 255:
                tup3 = 255

            tupla = (int(tup1), int(tup2), int(tup3))
            listo.append(tupla)
        imagen.append(listo)
    return imagen

# Parte opcional

def filtro_pixel(image: list[list[tuple[int, int, int]]], pixel_size: int) -> list[list[tuple[int, int, int]]]:
    pil_image = Image.new("RGB", (len(image[0]), len(image)))

    for y, row in enumerate(image):
        for x, pixel in enumerate(row):
            pil_image.putpixel((x, y), pixel)
    width, height = pil_image.size

    num_pixels_width = width // pixel_size
    num_pixels_height = height // pixel_size

    num_pixels_width = max(1, num_pixels_width)
    num_pixels_height = max(1, num_pixels_height)

    small_image = pil_image.resize((num_pixels_width, num_pixels_height), resample=Image.NEAREST)
    imagen_pixelada = small_image.resize((width, height), resample=Image.NEAREST)

    imagen = [[tuple(imagen_pixelada.getpixel((x, y))) for x in range(width)]for y in range(height)]

    return imagen


def bordes(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    pil_image = Image.new("RGB", (len(image[0]), len(image)))
    for y, row in enumerate(image):
        for x, pixel in enumerate(row):
            pil_image.putpixel((x, y), pixel)
    bordes_imagen = pil_image.filter(ImageFilter.FIND_EDGES)
    bordes_list = list(bordes_imagen.getdata())

    # Invertir los colores y reformatear la lista de bordes (Queda mas bonito que en blanco con los contornos negros)
    bordes = [
        tuple((255 - pixel[0], 255 - pixel[1], 255 - pixel[2])) for pixel in bordes_list
    ]
    bordes = [bordes[i:i+len(image[0])] for i in range(0, len(bordes), len(image[0]))]

    return bordes
def emboss(image: list[list[tuple[int, int, int]]], intensidad: float = 1.5) -> list[list[tuple[int, int, int]]]:
    imagen_pil = Image.new("RGB", (len(image[0]), len(image)))

    for y, row in enumerate(image):
        for x, pixel in enumerate(row):
            imagen_pil.putpixel((x, y), pixel)

    imagen_filtrada_pil = imagen_pil.filter(ImageFilter.EMBOSS)

    imagen_filtrada = [
        [tuple(int(value * intensidad) for value in imagen_filtrada_pil.getpixel((x, y))) for x in range(len(image[0]))]
        for y in range(len(image))
    ]

    return imagen_filtrada

def invertir_colores_imagen(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    imagen_invertida = [
        [(255 - pixel[0], 255 - pixel[1], 255 - pixel[2]) for pixel in row]
        for row in image
    ]
    return imagen_invertida