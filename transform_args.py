import sys
import images
import transforms

def main():
    try:
        nombre = sys.argv[1]
    except IndexError:
        print("Error: El nombre del archivo que has introducido no existe.")
        sys.exit(1)
    try:
        foto = nombre.split(".jpg")
    except:
        print("Error: El archivo que has introducido no tiene extension .jpg")
        sys.exit(1)

    opcion = sys.argv[2]
    image = images.read_img(nombre)

    if opcion == "change_colors":
        try:
            num1 = int(sys.argv[3])
            num2 = int(sys.argv[4])
            num3 = int(sys.argv[5])
            num4 = int(sys.argv[6])
            num5 = int(sys.argv[7])
            num6 = int(sys.argv[8])
        except IndexError:
            print("Error: Uno de los valores que has introducido, no son correctos o no son numeros enteros. ")
            sys.exit(1)
        tup1 = (num1, num2, num3)
        tup2 = (num4, num5, num6)
        imagen = transforms.change_colors(image, tup1, tup2)
    elif opcion == "rotate_colors":
        try:
            incremento = int(sys.argv[3])
        except IndexError:
            print("Error: Se te ha olvidado el valor del incremento de los colores. ")
            sys.exit(1)
        imagen = transforms.rotate_colors(image, incremento)

    elif opcion == "blur":
        imagen = transforms.blur(image)

    elif opcion == "filter":
        try:
            num1 = float(sys.argv[3])
            num2 = float(sys.argv[4])
            num3 = float(sys.argv[5])

        except IndexError:
            print("Error: Uno de los valores que has introducido, no son correctos o no son numeros enteros. ")
            sys.exit(1)
        imagen = transforms.filter(image, num1, num2, num3)
    else:
        print("Error: La opción que has introducido no existe.")
        sys.exit(1)

    images.write_img(imagen, f"{foto[0]}_trans_args.jpg")


if __name__ == '__main__':
    main()
