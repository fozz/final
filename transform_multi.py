import sys
import images
import transforms

def main():
    var = 0

    try:
        nombre = sys.argv[1]
        var += 1
    except IndexError:
        print("Error: El archivo que has introducido no existe.")
        sys.exit(1)

    foto = nombre.split(".jpg")
    numero = foto.count("")
    if numero < 1:
        print("Error: El archivo que has introducido no tiene extension .jpg")
        sys.exit(1)
    image = images.read_img(nombre)
    hola = len(sys.argv)-1
    while var < hola:
        opcion = sys.argv.pop(2)
        var += 1

        if opcion == "change_colors":
            try:
                num1 = int(sys.argv.pop(2))
                num2 = int(sys.argv.pop(2))
                num3 = int(sys.argv.pop(2))
                num4 = int(sys.argv.pop(2))
                num5 = int(sys.argv.pop(2))
                num6 = int(sys.argv.pop(2))
            except IndexError:
                print("Error: Uno de los valores que has introducido, no son correctos o no son numeros enteros. ")
                sys.exit(1)
            var += 6
            tup1 = (num1, num2, num3)
            tup2 = (num4, num5, num6)
            imagen = transforms.change_colors(image, tup1, tup2)

        elif opcion == "rotate_right":
            imagen = transforms.rotate_right(image)

        elif opcion == "mirror":
            imagen = transforms.mirror(image)

        elif opcion == "rotate_colors":
            try:
                incremento = int(sys.argv.pop(2))
            except IndexError:
                print("Error: Se te ha olvidado el valor del incremento de los colores.")
                sys.exit(1)
            var += 1
            imagen = transforms.rotate_colors(image, incremento)

        elif opcion == "blur":
            imagen = transforms.blur(image)

        elif opcion == "filter":
            try:
                num1 = float(sys.argv.pop(2))
                num2 = float(sys.argv.pop(2))
                num3 = float(sys.argv.pop(2))

            except IndexError:
                print("Error: Uno de los valores que has introducido, no es correcto o no es un numero.")
                sys.exit(1)
            var += 3
            imagen = transforms.filter(image, num1, num2, num3)

        elif opcion == "crop":
            try:
                value_x = int(sys.argv.pop(2))
                value_y = int(sys.argv.pop(2))
                ancho = int(sys.argv.pop(2))
                alto = int(sys.argv.pop(2))

            except IndexError:
                print("Error: Debes de introducir los valores para la corte de la imagen asi : x y ancho alto.")
                sys.exit(1)
            var += 4
            imagen = transforms.crop(image, value_x, value_y, ancho, alto)

        elif opcion == "shift":
            try:
                horizontal = int(sys.argv.pop(2))
                vertical = int(sys.argv.pop(2))
            except IndexError:
                print("Error: Los numeros deben de ser enteros.")
                sys.exit(1)
            var += 2
            imagen = transforms.shift(image, horizontal, vertical)

        elif opcion == "grayscale":
            imagen = transforms.grayscale(image)

        #Parte opcional de la practica
        elif opcion == "filtro_pixel":
            imagen = transforms.filtro_pixel(image, 10)
        elif opcion == "bordes":
            imagen = transforms.bordes(image)
        elif opcion == "invertir_colores_imagen":
            imagen = transforms.invertir_colores_imagen(image)
        elif opcion == "emboss":
            imagen = transforms.emboss(image)

        else:
            print("Error: La funcion que has introducido no existe.")
            sys.exit(1)
        image = imagen
        images.write_img(imagen, f"{foto[0]}_trans_multi.jpg")



if __name__ == '__main__':
    main()
